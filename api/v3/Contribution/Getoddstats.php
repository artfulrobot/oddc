<?php
use CRM_Oddc_ExtensionUtil as E;

/**
 * Contribution.Getoddstats API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see http://wiki.civicrm.org/confluence/display/CRMDOC/API+Architecture+Standards
 */
function _civicrm_api3_contribution_Getoddstats_spec(&$spec) {
  $spec['date_from']   = ['description' => 'Optional earliest date (not used by the report)'];
  $spec['date_to']     = ['description' => 'Optional latest date (not used by the report)'];
  $spec['granularity'] = ['description' => 'Date granularity (not used by the report - day used only)', 'options' => ['day', 'week', 'month', 'quarter', 'year'], 'default' => 'week'];
}

/**
 * Contribution.Getoddstats API
 *
 * @param array $params
 *
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function civicrm_api3_contribution_Getoddstats($params) {

  $set = $params['stats_set'] ?? 1;
  if ($set == 1) {
    return _oddStats1($params);
  }
  elseif ($set == 2) {
    return _oddStats2($params);
  }
  throw new API_Exception("Invalid 'stats_set'");
}
/**
 * Contribution.Getoddstats API
 *
 * @param array $params
 *
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function _oddStats1($params) {
  $result = [ ];
  $t = microtime(TRUE);

  if (!empty($params['test'])) {
    $result = [
      'contributions' => [
        // 0            1          2          3          4         5     6       7
        // period       campaign,  project    source_idx recur     nid   amount, contribs
        // Couple of one-offs.
        [ '2017-01-01', 1,        'Project A', 0,       'one-off', 1234, 10,     1 ],
        [ '2018-01-01', 2,        'Project B', 1,       'one-off', 1234, 10,     1 ],
        // Some regulars
        [ '2017-02-01', 1,        'Project B', 1,       'first',   1234, 10,     1 ],
        [ '2017-03-01', 1,        'Project B', 1,       'repeat',  1234, 10,     1 ],
        [ '2017-04-01', 1,        'Project B', 1,       'repeat',  1234, 10,     1 ],
        [ '2017-05-01', 1,        'Project B', 1,       'repeat',  1234, 10,     1 ],
        [ '2017-06-01', 1,        'Project B', 1,       'repeat',  1234, 10,     1 ],
        [ '2017-07-01', 1,        'Project B', 1,       'repeat',  1234, 10,     1 ],
        [ '2017-08-01', 1,        'Project B', 1,       'repeat',  1234, 10,     1 ],
        [ '2017-09-01', 1,        'Project B', 1,       'repeat',  1234, 10,     1 ],
        [ '2017-10-01', 1,        'Project B', 1,       'repeat',  1234, 10,     1 ],
        // Another regular
        [ '2017-06-01', 2,        'Project A', 2,       'first',   4567, 30,     2 ],
        [ '2018-01-01', 2,        'Project A', 2,       'repeat',  4567, 30,     2 ],
        [ '2019-01-01', 2,        'Project A', 2,       'repeat',  4567, 30,     2 ],
      ],
      'sources' => [
        [ 'name' => 'None'],
        [ 'name'=> 'Source A' ],
        [ 'name'=> 'Email A', 'opened_rate'=>"88.23%", 'clickthrough_rate'=> "2.56%", 'Delivered'=> "23414" ],
        [ 'name'=> 'Email B', 'opened_rate'=>"90.12%", 'clickthrough_rate'=> "10.56%", 'Delivered'=> "12323" ],
      ],
      'donation_pages' => [
        1234 => 'Test donation page',
        4567 => 'Other donation page',
      ],
      'campaigns' => [
        1 => 'Campaign A',
        2 => 'Campaign B',
      ],
      'recur' => [
        [
          'period' => '2017-01-01',
          'create_date' => [0, 0],
          'start_date'  => [0, 0],
          'cancel_date' => [0, 0],
          'end_date'    => [0, 0],
        ]
      ]
    ];
    return civicrm_api3_create_success($result, $params, 'Contribution', 'GetODDStats');
  }
  switch ($params['granularity'] ?? '') {
  case 'year':
    $sql_date_format = 'DATE_FORMAT(receive_date, "%Y")';
    $sql_display_format = $sql_date_format;
    break;

  case 'quarter':
    $sql_date_format = 'CONCAT(YEAR(receive_date), QUARTER(receive_date))';
    $sql_display_format = 'CONCAT("Q", QUARTER(receive_date) , " ", YEAR(receive_date))';
    break;

  case 'week':
    $sql_date_format = 'DATE_FORMAT(receive_date, "%x-%v")';
    $sql_display_format = 'DATE_FORMAT(receive_date, "W%x %v")';
    break;

  case 'day':
    $sql_date_format = 'DATE_FORMAT(receive_date, "%Y-%m-%d")';
    #$sql_display_format = 'DATE_FORMAT(receive_date, "%e %M %Y")';
    $sql_display_format = $sql_date_format;
    break;

  case 'month':
  default:
    $sql_date_format = 'DATE_FORMAT(receive_date, "%Y%m")';
    $sql_display_format = 'DATE_FORMAT(receive_date, "%M %Y")';

  }

  require_once 'CRM/Core/BAO/CustomField.php';
  $id = CRM_Core_BAO_CustomField::getCustomFieldID('od_project', 'od_project_group');
  list($table_name, $project_field_name) = CRM_Core_BAO_CustomField::getTableColumnGroup($id);
  $id = CRM_Core_BAO_CustomField::getCustomFieldID('donation_page_nid', 'od_project_group');
  list($table_name, $donation_page_nid_field_name) = CRM_Core_BAO_CustomField::getTableColumnGroup($id);

  $date_from_sql = '';
  if (!empty($params['date_from'])) {
    $_ = strtotime($params['date_from']);
    if ($_) {
      $date_from_sql = 'AND receive_date >= "' . date('Y-m-d', $_) . '"';
    }
  }
  $date_to_sql = '';
  if (!empty($params['date_to'])) {
    $_ = strtotime($params['date_to']);
    if ($_) {
      $date_to_sql = 'AND receive_date <= "' . date('Y-m-d', $_) . '"';
    }
  }


  $sql_params = [];
  $completed_status = (int) CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'Completed');

  $sql = "SELECT
      $sql_display_format period,
      campaign_id,
      proj.`$project_field_name` project,
      proj.`$donation_page_nid_field_name` donation_page_nid,
      source,
      IF(cc.contribution_recur_id IS NULL,
        'one-off',
        IF (EXISTS (
            SELECT id FROM civicrm_contribution cc_first
            WHERE cc.contribution_recur_id = cc_first.contribution_recur_id
              AND cc_first.contribution_status_id = $completed_status
              AND cc_first.id < cc.id
          ),
        'repeat',
        'first'
        )) recur,
      SUM(net_amount) amount,
      COUNT(*) contributions
    FROM civicrm_contribution cc
    LEFT JOIN `$table_name` proj ON proj.entity_id = cc.id
    WHERE is_test = 0
          AND cc.contribution_status_id = $completed_status
          $date_from_sql
          $date_to_sql
    GROUP BY $sql_date_format DESC, campaign_id, proj.`$project_field_name`, donation_page_nid, source, recur
  ";
  $dao = CRM_Core_DAO::executeQuery($sql, $sql_params);
  $campaign_ids = [];
  // Normalise the sources data.
  // Ensure the 0 index-ed item is for None; we know we'll need this.
  $unique_sources = ['(None)' => 0];

  $unique_pages = [];
  while ($dao->fetch()) {
    if ($dao->campaign_id) {
      $campaign_ids[$dao->campaign_id] = FALSE;
    }
    $source = $dao->source ? $dao->source : '(None)';
    if (!isset($unique_sources[$source])) {
      $unique_sources[$source] = count($unique_sources);
    }

    $nid = $dao->donation_page_nid ?? 0;
    $unique_pages[$nid] = 1;

    $result[] = [
      $dao->period,              // 0
      $dao->campaign_id,         // 1
      $dao->project,             // 2
      $unique_sources[$source],  // 3
      $dao->recur,               // 4
      $nid,                      // 5
      (double) $dao->amount,     // 6
      (int) $dao->contributions, // 7
    ];
  }
  $dao->free();
  $result = ['contributions' => $result];
  Civi::log()->info('Took ' . number_format(microtime(TRUE) - $t, 2) . 's to load main data SQL');
  $t = microtime(TRUE);

  // Now provide a campaign lookup table.
  $campaigns = [];
  if ($campaign_ids) {
    $campaign_fetch = civicrm_api3('Campaign', 'get', [
      'id' => ['IN' => array_keys($campaign_ids)],
      'options' => ['limit' => 0],
      'return' => ['id', 'title'],
    ]);
    foreach ($campaign_fetch['values'] as $_) {
      $campaigns[$_['id']] = $_['title'];
    }
  }
  $result['campaigns'] = $campaigns;
  Civi::log()->info('Took ' . number_format(microtime(TRUE) - $t, 2) . 's to load campaigns');
  $t = microtime(TRUE);

  // Add internal name of donation pages (Drupal query.)
  $result['donation_pages'] = [];
  // 2025-02-06 with standalone drupal and its nids are gone.
  $nidToTitleMap = [
    2 => "Rich's demo page",
    8 => "Main default donation page",
    9 => "Dark money donation page (main site/oDUK banners)",
    10 => "Transformation default page",
    11 => "50.50 donation page",
    12 => "Beyond Trafficking and Slavery donation page",
    13 => "Can Europe Make It? donation page",
    14 => "democraciaAbierta English donation page",
    15 => "North Africa, West Asia donation page",
    16 => "oDR donation page",
    17 => "openDemocracyUK donation page",
    18 => "openJustice donation page",
    19 => "ourNHS donation page",
    20 => "Shine A Light donation page",
    22 => "Test  Page",
    23 => "World Congress of Families 2019 - narrative for socials",
    24 => "World Congress of Families 2019 - for website and newsletter",
    26 => "Default donation page",
    28 => "[AB-Test-Double] Main default donation page",
    30 => "Default donation page [AB Test duplicate]",
    31 => "ourEconomy default donation page",
    32 => "Default to openDemocracy",
    33 => "Upgraders",
    34 => "Tracking the Backlash",
    35 => "DemocracyWatch sign-up form and donation page",
    39 => "yourData donations",
    41 => "oD-Foxglove NHS data deals one-off donation",
    44 => "17072020 TTB COVID Childbirth donate ",
    45 => "20201026 TTB US Christian-right Investigation",
    46 => "20201123 UK FOI Donations",
    47 => "FOI campaign petition Nov 2020",
    48 => "US sign-up form and donation page",
    49 => "20201214 oD-Foxglove NHS data legal action one-off donation",
    52 => "Foreign aid cuts campaign",
    53 => "Dont sell off our GP data",
    57 => "08062021 FOI Win",
    58 => "Make sure you’re not funding anti-gay ‘conversion therapy’",
    59 => "Electoral reform/dark money petition",
    60 => "Ukraine support donation page",
    61 => "Daily email signup page",
    62 => "Climate Unspun donation page",
    64 => "democraciaAbierta español donation page",
    65 => "democraciaAbierta portugues donation page",
    66 => "Nazarbayev case core supporters donation page",
    67 => "Covid 19 inquiry",
    68 => "ODR 2023 appeal",
  ];
  foreach (array_keys($unique_pages) as $nid) {
      $result['donation_pages']["nid$nid"] = $nidToTitleMap[$nid] ?? 'Unknown';
  }
  Civi::log()->info('Took ' . number_format(microtime(TRUE) - $t, 2) . 's to load donation pages');
  $t = microtime(TRUE);


  // Add Email stats.
  $mailing_ids = [];
  foreach ($unique_sources as $text => $count) {
    if (preg_match('/^mailing(\d+)$/', $text, $matches)) {
      $mailing_ids[$text] = $matches[1];
    }
  }

  /*
  // Load all mailings found in sources.
  $mailings = [];
  if ($mailing_ids) {
    $mailings = civicrm_api3('Mailing', 'get', [
      'return'       => ["name"],
      'id'           => ['IN' => array_values($mailing_ids)],
      'options'      => ['limit' => 0],
    ]);
    $mailings = $mailings['values'] ?? [];
  }
  Civi::log()->info('Took ' . number_format(microtime(TRUE) - $t, 2) . 's to load mailings');
  $t = microtime(TRUE);
   */

  // Create the source lookup data.
  $result['sources'] = [];
  foreach ($unique_sources as $name => $idx) {
    // IF $name is a mailing. REMOVED {{{
    if (FALSE && isset($mailing_ids[$name]) && isset($mailings[$mailing_ids[$name]])) {
      // This is a mailing and we have been able to load it's name.
      $mailing_id = (int) $mailing_ids[$name];
      $mailing = $mailings[$mailing_id];
      // Make the name a bit nicer.
      $result['sources'][$idx] = [
        'name' => $name . ': ' . $mailing['name'],
      ];
      // Add in the stats.
      $stats = civicrm_api3('Mailing', 'stats', ['mailing_id' => $mailing_id]);
      if (!empty($stats['values'][$mailing_id])) {
        $result['sources'][$idx] += $stats['values'][$mailing_id];
      }

      $result['sources'][$idx]['unique_opens'] =
        CRM_Core_DAO::executeQuery("SELECT count(distinct q.contact_id)
          FROM civicrm_mailing_event_opened o
          INNER JOIN  civicrm_mailing_event_queue q ON o.event_queue_id = q.id
          INNER JOIN civicrm_mailing_job j ON q.job_id = j.id
          WHERE j.mailing_id = $mailing_id;")->fetchValue();

      // Replace CiviCRM's opened_rate which is not unique opens and therefore pretty useless.
      $result['sources'][$idx]['opened_rate'] = number_format(100 * $result['sources'][$idx]['unique_opens'] / $result['sources'][$idx]['Delivered'], 2) . '%';

    } // }}}
    else {
      // Not a mailing, just output the source name as is.
      $result['sources'][$idx] = [ 'name' => $name, ];
    }
  }
  Civi::log()->info('Took ' . number_format(microtime(TRUE) - $t, 2) . 's to load source metadata');
  $t = microtime(TRUE);

  // Get joiners and leavers from the contrib recur.
  $sql = "SELECT
      $sql_display_format period,
      SUM(amount) amount,
      COUNT(*) people
    FROM civicrm_contribution_recur cr
    WHERE is_test = 0 $date_from_sql $date_to_sql
      AND $sql_display_format IS NOT NULL AND receive_date IS NOT NULL
    GROUP BY $sql_date_format DESC
    ORDER BY $sql_date_format DESC
  ";
  $result['recur'] = [];
  foreach (['create_date', 'start_date', 'cancel_date', 'end_date'] as $field) {
    $sql1 = str_replace('receive_date', $field, $sql);
    $dao = CRM_Core_DAO::executeQuery($sql1, $sql_params);
    while ($dao->fetch()) {
      if (!isset($result['recur'][$dao->period])) {
        $result['recur'][$dao->period] = [
          'period' => $dao->period,
          'create_date' => [0, 0],
          'start_date'  => [0, 0],
          'cancel_date' => [0, 0],
          'end_date'    => [0, 0],
        ];
      }
      $result['recur'][$dao->period][$field] = [(double)$dao->amount, (int)$dao->people];
    }
  }
  $result['recur'] = array_values($result['recur']);
  Civi::log()->info('Took ' . number_format(microtime(TRUE) - $t, 2) . 's to load joiners/leavers');


  return civicrm_api3_create_success($result, $params, 'Contribution', 'GetODDStats');
  //throw new API_Exception(/*errorMessage*/ 'Everyone knows that the magicword is "sesame"', /*errorCode*/ 1234);
}
/**
 * Contribution.Getoddstats API
 *
 * @param array $params
 *
 * - nocache If set to 1 then the cache is not used. Normally the results are
 *           cached if they are for full calendar months.
 * - months  Default: 48. Months to go back to generate stats for.
 *
 * - stats   If set to an array of stat names, only these are calculated/returned.
 *           Otherwise all.
 *
 * @return array API result descriptor
 * @see civicrm_api3_create_success
 * @see civicrm_api3_create_error
 * @throws API_Exception
 */
function _oddStats2($params) {
  $result = [];
  $x = new CRM_Oddc_Stats(); // xxx include
  if ($params['list'] ?? NULL) {
    $sx = new Statx();
    $result = $sx->listStats();
    return civicrm_api3_create_success($result, $params, 'Contribution', 'GetODDStats');
  }
  Civi::log()->debug("_oddStats2", ['=start' => 'oddStats2', '=timed' => 1]);

  $today = date('Y-m-d');

  // Get first of this month, a year ago
  $months = empty($params['months']) ? 48 : (int) $params['months'];
  // $params['nocache'] = 1;
  $given = new DateTimeImmutable("today - $months month");
  $startOfMonth = $given->modify('first day of');
  $endOfMonth = $startOfMonth->modify('+1 month - 1 second');
  $now = new DateTimeImmutable('today');

  $months = [];
  while ($startOfMonth < $now) {
    $months[] = [$startOfMonth->format('c'), $endOfMonth->format('c')];
    $startOfMonth = $startOfMonth->modify('+1 month');
    $endOfMonth = $startOfMonth->modify('+1 month -1 second');
  }

  $cache = CRM_Utils_Cache::create(['type' => ['SqlGroup'], 'name' => 'revenuedashboard']);
  //$value = $cache->get($cache_key, 'default'); // Will return default if cached value expired.
  //$cache->set($cache_key, $data, 60); // Keep value for 60s

  foreach ($months as $month) {
    Civi::log()->debug("Doing month $month[0] - $month[1]", ['=' => 'start,timed']);
    // Append either 'incomplete' or 'full' to the month specification.
    $month[] = ($month[1] > $today) ? 'incomplete' : 'full';

    $monthStats = NULL;
    $loadedFromCache = FALSE;
    $cacheKey = "$month[0]-$month[1]";
    if ($month[2] === 'full') {
      if (empty($params['nocache'])) {
        $monthStats = $cache->get($cacheKey, NULL);
        $loadedFromCache = (bool) $monthStats;
      }
    }


    if (!$monthStats) {
      $sx = new Statx([
        'startDate' => $month[0],
        'endDate' => $month[1],
      ]);
      $statsToFetch = is_array($params['stats']) ? $params['stats'] : NULL;
      $monthStats = $sx->get($statsToFetch);
      $monthStats['period'] = $month;

      // Cache the full results indefinitely (until explicit cache clear) but no
      // point doing this if we already fetched them from the cache.
      if (!$loadedFromCache && $statsToFetch === NULL) {
        $cache->set($cacheKey, $monthStats);
      }
    }

    $result[] = $monthStats;
    Civi::log()->debug("Done month", ['=' => 'pop']);
  }

  Civi::log()->debug("complete", ['=pop'=> 1]);
  return civicrm_api3_create_success($result, $params, 'Contribution', 'GetODDStats');
  //throw new API_Exception(/*errorMessage*/ 'Everyone knows that the magicword is "sesame"', /*errorCode*/ 1234);
}
