<?php
/**
 * Example use:
 *
 *   // Call with -t to output to commandline and prevent email being sent.
 *   $reporter = new CustomEmailReport(($argv[1] ?? '') === '-t');
 *   $reporter->generateChurnLossReport();
 *
 *   $reporter->sendTo = 'wilma@example.com';
 *   $reporter->sendCC = 'a@example.org,b@example.org';
 *   $reporter->sendBCC = 'c@example.org';
 *   $reporter->output();
 *
 */

require_once dirname(__DIR__) . '/vendor/autoload.php';

class CustomEmailReport {

  public string $sendTo = '';
  public string $sendCC = '';
  public string $sendBCC = '';

  public array $md;

  public function __construct(public $testMode = FALSE) {
  }

  /**
   * Donors give monthly, at any time of the month. Also, with Direct Debit, a payment
   * is requested/submitted, but it's only 'confirmed' after 'enough time has passed
   * that it can be considered confirmed' which can be up to a week.
   *
   * Therefore we identify lost donors by finding those whose last donation was:
   * 31 days + 7 days (for delayed confirmation) = 38 days ago.
   *
   * The 38+7 day cut-off means we only report on a 1 week window of cancellations at a time,
   * since this script is supposed to be run Monday mornings each week.
   *
   */
  public function generateChurnLossReport() {

    $sql = <<<SQL
      SELECT  cn.contact_id, MIN(display_name) who,
              max(DATE(receive_date)) lastDonation, sum(total_amount) contactLifetimeTotal,
              cr.amount monthlyAmount,
              GROUP_CONCAT(DISTINCT cm.title SEPARATOR ', ') campaign,
              GROUP_CONCAT(DISTINCT cn.source SEPARATOR ', ') source,
              GROUP_CONCAT(DISTINCT ppt.title SEPARATOR ', ') pps,
              JSON_OBJECT(
                'campaign', GROUP_CONCAT(DISTINCT COALESCE(utmi.utm_campaign, '') SEPARATOR ', '),
                'medium', GROUP_CONCAT(DISTINCT COALESCE(utmi.utm_medium, '') SEPARATOR ', '),
                'source', GROUP_CONCAT(DISTINCT COALESCE(utmi.utm_source, '') SEPARATOR ', '),
                'term', GROUP_CONCAT(DISTINCT COALESCE(utmi.utm_term, '') SEPARATOR ', '),
                'content', GROUP_CONCAT(DISTINCT COALESCE(utmi.utm_content, '') SEPARATOR ', ')
              ) utmi_data
      FROM civicrm_contribution cn
      INNER JOIN civicrm_contribution_recur cr ON cr.id = contribution_recur_id
      INNER JOIN civicrm_contact ct ON cn.contact_id = ct.id
      LEFT JOIN civicrm_campaign cm ON cm.id = cn.campaign_id
      LEFT JOIN civicrm_value_inlaypay_contrib utmi ON utmi.entity_id = cn.id
      LEFT JOIN civicrm_payment_processor pp ON pp.id = cr.payment_processor_id
      LEFT JOIN civicrm_payment_processor_type ppt ON ppt.id = pp.payment_processor_type_id
      WHERE cn.contribution_status_id=1 AND cn.is_test = 0
      GROUP BY cn.contact_id
      HAVING lastDonation BETWEEN CURRENT_DATE - INTERVAL (38+7) DAY AND CURRENT_DATE - INTERVAL 38 DAY
      ORDER BY contactLifetimeTotal DESC, monthlyAmount DESC, lastDonation
      SQL;
    $dao = CRM_Core_DAO::executeQuery($sql);
    $date1 = date('j M', strtotime('today - 45 days'));
    $date2 = date('j M Y', strtotime('today - 38 days'));

    $churners = '';
    $countDonors = $totalMonthly = 0;
    while ($dao->fetch()) {
      $totalMonthly += (float) $dao->monthlyAmount;
      $countDonors++;
      $ctUrl = CRM_Utils_System::url('civicrm/contact/view', ['reset' => 1, 'cid' => $dao->contact_id], TRUE, FALSE);
      $churners .= "- **£" . number_format($dao->monthlyAmount, 2) . "/month**. Total given: **£" . number_format($dao->contactLifetimeTotal, 2) . "** "
        . ' by [' . strip_tags($dao->who) . "]($ctUrl)  \n"
        . ($dao->campaign ? '    Campaign: ' . $dao->campaign . "  \n" : '')
        . ($dao->source ? '    Source: ' . htmlspecialchars($dao->source) . "  \n" : '');
      if ($dao->utmi_data) {
        $utmi = json_decode($dao->utmi_data, TRUE);
        if (implode("", $utmi) !== '') {
          foreach ($utmi as $utmKey => $v) {
            if ($v) {
              $churners .= "    UTM $utmKey: " . htmlspecialchars($v) . "  \n";
            }
          }
        }
      }
      $churners .= '    Last donation: ' . date('j M Y', strtotime($dao->lastDonation))
        . ' by ' . $dao->pps . "\n\n";
      ;
    }

    $churners = $churners ?: "(No lost donors. Woop woop.)";
    $totalMonthly = number_format($totalMonthly, 2);

    $this->md[] = <<<MARKDOWN
      ## Churn loss of $countDonors regular donors $date1 - $date2

      The following regular donors appear to have stopped their donations because their last donation fell within this
      period; had they not stopped donating we would expect to have had a donation since $date2 but we haven't.

      The total monthly loss is £$totalMonthly

      $churners


      MARKDOWN;
  }

  /**
   * Generate a list of new regular donors.
   *
   * Note that this may also include existing donors who have set up a new/extra
   * regular giving subscription.
   */
  public function generateChurnGainReport() {

    $sql = <<<SQL

      WITH givingByContact AS (
        SELECT contact_id, SUM(total_amount) contactLifetimeTotal
        FROM civicrm_contribution cn
        WHERE is_test = 0 AND contribution_status_id = 1 AND is_template = 0
        GROUP BY contact_id
      ),

      newContribution AS (
        SELECT contribution_recur_id, MIN(source) source,
            JSON_OBJECT(
              'campaign', COALESCE(utmi.utm_campaign, ''),
              'medium', COALESCE(utmi.utm_medium, ''),
              'source', COALESCE(utmi.utm_source, ''),
              'term', COALESCE(utmi.utm_term, ''),
              'content', COALESCE(utmi.utm_content, '')
            ) utmi_data
        FROM civicrm_contribution
        LEFT JOIN civicrm_value_inlaypay_contrib utmi ON utmi.entity_id = civicrm_contribution.id
        WHERE is_template = 0
        GROUP BY contribution_recur_id
      ),

      crsByContact AS (
        SELECT contact_id, COUNT(*) c
        FROM civicrm_contribution_recur cr
        WHERE is_test = 0 AND contribution_status_id IN (5, 6) /* In Progress, Overdue */
        GROUP BY contact_id
      )

      SELECT  cr.contact_id, display_name who,
              cr.amount monthlyAmount,
              cm.title campaign,
              ppt.title pps,
              DATE(cr.start_date) start_date,
              COALESCE(givingByContact.contactLifetimeTotal, 0) contactLifetimeTotal,
              newContribution.source source,
              newContribution.utmi_data utmi_data,
              COALESCE(crsByContact.c, 1) - 1 otherCrs
      FROM civicrm_contribution_recur cr
      INNER JOIN newContribution ON newContribution.contribution_recur_id = cr.id
      INNER JOIN civicrm_contact ct ON cr.contact_id = ct.id
      LEFT JOIN civicrm_campaign cm ON cm.id = cr.campaign_id
      LEFT JOIN civicrm_payment_processor pp ON pp.id = cr.payment_processor_id
      LEFT JOIN civicrm_payment_processor_type ppt ON ppt.id = pp.payment_processor_type_id
      LEFT JOIN givingByContact ON givingByContact.contact_id = cr.contact_id
      LEFT JOIN crsByContact ON crsByContact.contact_id = cr.contact_id
      WHERE cr.contribution_status_id = 5 /* In Progress */ AND cr.is_test = 0
      AND cr.start_date >= CURRENT_DATE - INTERVAL 7 DAY
      ORDER BY contactLifetimeTotal DESC, cr.amount DESC

      SQL;
    $dao = CRM_Core_DAO::executeQuery($sql);
    $date1 = date('j M', strtotime('today - 7 days'));

    $joiners = '';
    $countDonors = $totalMonthly = 0;
    while ($dao->fetch()) {
      $totalMonthly += (float) $dao->monthlyAmount;
      $countDonors++;
      $ctUrl = CRM_Utils_System::url('civicrm/contact/view', ['reset' => 1, 'cid' => $dao->contact_id], TRUE, FALSE);
      $known = ($dao->contactLifetimeTotal > 0)
        ? "Total given to-date: **£" . number_format($dao->contactLifetimeTotal, 2) . "**"
        : "**New donor**";

      $joiners .= "- **£" . number_format($dao->monthlyAmount, 2) . "/month**. $known "
        . ' by [' . strip_tags($dao->who) . "]($ctUrl)  \n"
        . ($dao->campaign ? '    Campaign: ' . $dao->campaign . "  \n" : '')
        . ($dao->source ? '    Source: ' . htmlspecialchars($dao->source) . "  \n" : '');

      if ($dao->utmi_data) {
        $utmi = json_decode($dao->utmi_data, TRUE);
        if (implode("", $utmi) !== '') {
          foreach ($utmi as $utmKey => $v) {
            if ($v) {
              $joiners .= "    UTM $utmKey: " . htmlspecialchars($v) . "  \n";
            }
          }
        }
      }
      $joiners .= '    Start date: ' . date('j M Y', strtotime($dao->start_date))
        . ' by ' . $dao->pps . "\n"
        . (($dao->otherCrs > 0) ? "    **Contact has $dao->otherCrs other regular payments set up**\n" : '')
        . "\n\n";
      ;
    }

    $joiners = $joiners ?: "(No new donors. Boo.)";
    $totalMonthly = number_format($totalMonthly, 2);

    $this->md[] = <<<MARKDOWN
      ## Churn gain of $countDonors regular donors since $date1

      The following new regular giving subscriptions have been set up.

      The total monthly gain is £$totalMonthly

      $joiners


      MARKDOWN;
  }

  public function output() {
    $md = implode("\n", $this->md);

    if ($this->testMode) {
      fwrite(STDOUT, $md);
    }
    else {
      $this->generateFooter();
      $parsedown = new ParsedownExtra();
      $html = $parsedown->text($md);
      $html = <<<HTML
      <div id="email-donation-stats">$html</div>
      <style>
        #email-donation-stats {
          font-family: 'Open Sans', 'Lato', Arial, Helvetica, sans-serif;
        }
        h1, h2, h3 { font-family: inherit; }

      </style>

      HTML;

      $params = [
        'from'      => '"oD CiviCRM" <no-reply@support.opendemocracy.net>',
        'toEmail'   => $this->sendTo,
        'cc'        => $this->sendCC,
        'bcc'       => $this->sendBCC,
        'subject'   => 'Weekly CRM stats',
        'html'      => $html,
      ];
      $success = \CRM_Utils_Mail::send($params);
      if (!$success) {
        fwrite(STDERR, "email-donation-stats send failed\n");
      }
    }
  }

  protected function generateFooter() {
    $this->md[] = <<<MARKDOWN

    --

    This email is generated automatically by the CiviCRM server. If anything is unclear,
    could be improved, or you want to stop receiving this, please discuss with Matt Linares.

    MARKDOWN;
  }

}
