<?php

use Civi\Api4\ContributionRecur;

if (PHP_SAPI !== 'cli') {
  http_response_code(404);
  echo "404 Not Found.";
  exit;
}

$allData = CRM_Core_DAO::executeQuery(<<<SQL

  SELECT receive_date, contact_id, contribution_recur_id
  FROM civicrm_contribution cn
  WHERE contribution_recur_id IS NOT NULL
        AND cn.contribution_status_id = 1
        AND receive_date > 20230101
  ORDER BY contribution_recur_id, receive_date
  limit 500;

SQL);
$svg = '';

$cr = 0;
$y = 0;
$n = 0;
$mx = 0;
while ($allData->fetch()) {
  $cnDate =  new DateTimeImmutable($allData->receive_date);
  if ($cr !== $allData->contribution_recur_id) {
    // new one.
    $y += 10;
    $cr = $allData->contribution_recur_id;
    $s = $cnDate;
    $n = 1;
  }
  $x = 2* ((int) ($cnDate->diff($s)->format('%a')));
  $t = $cnDate->format('j');
  $svg .= "<circle cx=\"$x\" cy=\"$y\" r=\"1\" /><text x=\"$x\" y=\"$y\" >$t</text>\n";
  $n++;
  $mx = max($mx, $x);
}

$w = $mx + 20 * 2;
$h = $y + 20 * 2;
echo <<<HTML
<svg viewBox="-20 -20 $w $h" style="width:100%; height: auto;">
  <style>
    circle { fill: #ff9900;}
    text { font-size: 4px;}
  </style>
  $svg
</svg>
HTML;

exit;



$today = new DateTimeImmutable('today');
$ptr = $today->sub(new DateInterval('P1W'));;
$endOfExpectedPeriod = $today; // Special for this month.
$startOfExpectedPeriod = $ptr->sub(new DateInterval('P31D'));


$crs = ContributionRecur::get(FALSE)
  ->addSelect('contact_id', 'start_date', 'end_date', 'cancel_date', 'contribution_status_id:name')
  ->addWhere('start_date', '>=', '20200101')
  ->addWhere('start_date', '<=', '20240101')
  ->addOrderBy('start_date', 'DESC')
  ->setLimit(40)
  ->execute();

foreach ($crs as $cr) {

  $end = substr(($cr['end_date'] ?? '') ?: ($cr['cancel_date'] ?? '          '), 0, 10);
  print "cr$cr[id] " . substr($cr['start_date'], 0, 10) . " - $end: ";

  print "\n";

  while ($ptr->format('Y-m') >= substr($cr['start_date'], 0, 8)) {
    $short = $ptr->format('M Y');

    // Was there a contribution in expected period?
    $startOfExpectedPeriodYmd = $startOfExpectedPeriod->format('Ymd');
    $endOfExpectedPeriodYmd = $endOfExpectedPeriod->format('Ymd');
    $cns = CRM_Core_DAO::executeQuery(<<<SQL
      SELECT id, receive_date
      FROM civicrm_contribution cn
      WHERE receive_date BETWEEN $startOfExpectedPeriodYmd AND $endOfExpectedPeriodYmd
      AND contribution_recur_id = $cr[id]
      AND contribution_status_id = 1
      ;
    SQL)->fetchAll();

    print "? $short: $startOfExpectedPeriodYmd - $endOfExpectedPeriodYmd: ";
    if (!$cns) {
      // not current.
      print "×";
    }
    else {
      print count($cns);
    }
    print "\n";

    // Go back a month.
    $ptr = $ptr->sub(new DateInterval('P31D'));
    $startOfExpectedPeriod = $ptr->sub(new DateInterval('P31D'));
    $endOfExpectedPeriod = $ptr;
  }
  print"\n";

  $cns = CRM_Core_DAO::executeQuery(<<<SQL
      SELECT id, receive_date
      FROM civicrm_contribution cn
      WHERE contribution_recur_id = $cr[id]
      AND contribution_status_id = 1
      ORDER BY receive_date DESC
      ;
    SQL)->fetchAll();
  foreach ($cns as $cn) {
    print '  ' . substr($cn['receive_date'], 0, 10) . "\n";
  }

  print "\n";

  $ptr = $today->sub(new DateInterval('P1W'));
  $startOfExpectedPeriod = $ptr->sub(new DateInterval('P31D'));
  $endOfExpectedPeriod = $ptr;
}

