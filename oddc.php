<?php

require_once 'oddc.civix.php';

use Civi\Api4\Contribution;
use CRM_Oddc_ExtensionUtil as E;
use Civi\Core\Event\GenericHookEvent;
use Civi\Inlay\Pay;

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function oddc_civicrm_config(&$config) {
  _oddc_civix_civicrm_config($config);

  // Prevent us registering 2+ times.
  if (isset(Civi::$statics[__FUNCTION__])) {
    return;
  }
  Civi::$statics[__FUNCTION__] = 1;
  // https://docs.civicrm.org/dev/en/latest/hooks/usage/symfony/
  Civi::dispatcher()->addListener(Civi\API\Events::RESPOND, 'oddc__wrap_mailing_preview');
  Civi::dispatcher()->addListener('civi.inlaysignup.process', 'oddc_inlaysignup_process_handler');
  if (class_exists(Pay::class)) {
    Civi::dispatcher()->addListener(Pay::EVENT_CHECKOUT_BEGIN, 'oddc_inlaypay_checkoutbegin');
    Civi::dispatcher()->addListener(Pay::EVENT_SUCCESS_PRE, 'oddc_inlaypay_successpre');
    Civi::dispatcher()->addListener(Pay::EVENT_SUCCESS_POST, 'oddc_inlaypay_successpost');
  }
  // Civi::dispatcher()->addListener(CRM_Core_Payment_GoCardless::EVENT_BRF_URL_ALTER, 'oddc_gocardless_brf_alter');
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function oddc_civicrm_install() {
  _oddc_civix_civicrm_install();

  /**
   * Helper function for creating data structures.
   *
   * @param string $entity - name of the API entity.
   * @param Array $params_min parameters to use for search.
   * @param Array $params_extra these plus $params_min are used if a create call
   *              is needed.
   */
  $api_get_or_create = function ($entity, $params_min, $params_extra) {
    $params_min += ['sequential' => 1];
    $result = civicrm_api3($entity, 'get', $params_min);
    if (!$result['count']) {
      Civi::log()->notice('get_or_create Could not find entity, creating now', ['entity' => $entity, 'min' => $params_min, 'extra' => $params_extra]);
      // Couldn't find it, create it now.
      $result = civicrm_api3($entity, 'create', $params_extra + $params_min);
      // reload
      $result = civicrm_api3($entity, 'get', $params_min);
    }
    else {
      Civi::log()->notice('get_or_create Found entity', ['entity' => $entity, 'min' => $params_min, 'found' => $result['values'][0]]);
    }
    return $result['values'][0];
  };

  // Ensure we have the marketing_consent activity type.
  $consent_activity = $api_get_or_create(
    'OptionValue',
    ['option_group_id' => "activity_type", 'name' => "marketing_consent"],
    [
      'label'           => "Consent",
      'description'     => "Records a log of this contact having given marketing consent to help comply with the GDPR.",
    ]
  );

  // Ensure we have the custom field group we need for project.
  $contribution_custom_group = $api_get_or_create(
    'CustomGroup',
    [
      'name' => "od_project_group",
      'extends' => "Contribution",
    ],
    ['title' => 'oD Project']
  );

  // Add our 'Project' field.
  // ...This is a drop-down select field, first we need to check the option
  //    group exists, and its values.
  $opts_group = $api_get_or_create(
    'OptionGroup',
    ['name' => 'od_project_opts'],
    ['title' => 'oD Project', 'is_active' => 1]
  );
  $weight = 0;
  foreach ([
    "50.50"                          => "50.50",
    "Beyond Trafficking and Slavery" => "Beyond Trafficking and Slavery",
    "Can Europe Make It?"            => "Can Europe Make It?",
    "Dark Money Investigations"      => "Dark Money Investigations",
    "democraciaAbierta"              => "democraciaAbierta",
    "DigitaLiberties"                => "DigitaLiberties",
    "North Africa, West Asia"        => "North Africa, West Asia",
    "oDR"                            => "oDR",
    "openDemocracyUK"                => "openDemocracyUK",
    "openJustice"                    => "openJustice",
    "openMedia"                      => "openMedia",
    "openMigration"                  => "openMigration",
    "ourBeeb"                        => "ourBeeb",
    "ourNHS"                         => "ourNHS",
    "Shine a Light"                  => "Shine a Light",
    "Transformation"                 => "Transformation",
  ] as $name => $label) {
    $api_get_or_create(
      'OptionValue',
      ['option_group_id' => "od_project_opts", 'name' => $name],
      ['label' => $label, 'value' => $name, 'weight' => $weight++]
    );
  }

  // ... Now we can add the Project field to the custom group for contributions.
  $project = $api_get_or_create(
    'CustomField',
    [
      'name' => "od_project",
      'custom_group_id' => $contribution_custom_group['id'],
      'data_type' => "String",
      'html_type' => "Select",
      'is_required' => "1",
      'is_searchable' => "1",
      'default_value' => "unknown",
      'text_length' => "30",
      'option_group_id' => $opts_group['id'],
    ],
    ['label' => 'oD Project']
  );

  // ... we also need to add donation_page_nid field. @todo
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function oddc_civicrm_enable() {
  _oddc_civix_civicrm_enable();
}

/**
 * Uses hook_civicrm_post to update campaign targets after saving a contribution.
 */
function oddc_civicrm_post($op, $objectName, $objectId, &$objectRef) {
  if ($op === 'view' || $objectName !== 'Contribution') {
    return;
  }
  // Contribution has been changed. Update the stats.
  CRM_Oddc::factory()->updateCampaignTargetStats();
}

/**
 * User has set up mandate at GC, complete this process and set up subscription.
 *
 * Called by Drupal's node preprocesshook
 *
 */
function oddc__complete_go_cardless_billing_request_flow($input) {
  return CRM_Oddc::factory($input)->completeGoCardlessBillingRequestFlow($input);
}

/**
 * User has successfully made a payment and clicked Return To Merchant on the
 * PayPal page.
 *
 * @input array $input (POST data)
 */
function oddc__complete_paypal($input) {
  return CRM_Oddc::factory()->completePayPal($input);
}

/**
 * Augment the app config array with details loaded from a contact's record, if
 * the checksum is valid.
 */
function oddc__add_contact_data_from_checksum(&$odd_app_config, $cid, $cs) {

  // Set default empty giving string.
  $odd_app_config['giving'] = '';

  $cid = (int) $cid;
  if (!$cid > 0) {
    // Suspicious.
    return;
  }
  if (!CRM_Contact_BAO_Contact_Utils::validChecksum($cid, $cs)) {
    // Invalid checksum.
    return;
  }
  // Checksum is, load contact details.
  $fields = ["email", "first_name", 'last_name', "street_address", "city", "postal_code", "country_id"];
  $contact = civicrm_api3('Contact', 'getsingle', ['id' => $cid, 'return' => $fields]);

  // We need an ISO 3166-1 alpha-2 version of the country, not the CiviCRM country ID.
  if (!empty($contact['country_id'])) {
    $contact['country'] = CRM_Core_PseudoConstant::countryIsoCode($contact['country_id']);
  }

  // remove country_id, add country.
  array_pop($fields);
  $fields[] = 'country';

  // Look up current giving
  $odd_app_config['giving'] = CRM_Oddc::factory()->getCurrentRegularGivingDescription($cid);

  // Copy any data we have into the app config.
  foreach ($fields as $_) {
    if (!empty($contact[$_])) {
      $odd_app_config[$_] = $contact[$_];
    }
  }
}

/**
 * Augment the app config array with details loaded campaign funding.
 *
 * @param Array $odd_app_config
 * @param String $campaign_name
 */
function oddc__add_campaign_funding_data(&$odd_app_config, $campaign_name) {
  $result = civicrm_api3('Campaign', 'get', [
    'sequential' => 1,
    'return'     => [CRM_Oddc::API_CUSTOM_FIELD_CAMPAIGN_FUNDING_TARGET, CRM_Oddc::API_CUSTOM_FIELD_CAMPAIGN_FUNDING_RCVD],
    'name'       => $campaign_name,
  ]);
  $odd_app_config['campaign_target'] = (int) ($result['values'][0][CRM_Oddc::API_CUSTOM_FIELD_CAMPAIGN_FUNDING_TARGET] ?? 0);
  $odd_app_config['campaign_total']  = (int) ($result['values'][0][CRM_Oddc::API_CUSTOM_FIELD_CAMPAIGN_FUNDING_RCVD] ?? 0);
}

/**
 * Return a map of countries.
 */
function oddc__get_country_list() {
  $result = civicrm_api3('Country', 'get', ['return' => ["iso_code", "name"], 'options' => ['limit' => 0, 'sort' => 'name']]);
  $list = [];
  foreach ($result['values'] as $_) {
    $list[$_['iso_code']] = $_['name'];
  }
  return $list;
}

/**
 * Implements hook_civicrm_alterMailParams
 * https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterMailParams/
 *
 * Used to abort sending payment receipt for odd Donation Pages.
 *
 * $params['abortMailSend'] = TRUE;
 *
 * @param array &$params
 * @param array $context
 */
function oddc_civicrm_alterMailParams(&$params, $context) {

  // 2023-06-15 I've decided to keep this simple by just not sending receipts or other notifications.
  if ($context === 'messageTemplate' && in_array($params['workflow'] ?? '', [
    'contribution_recurring_notify',
    'contribution_online_receipt',
  ])) {
    $params['abortMailSend'] = TRUE;
  }
  return;

  // if ( ($params['groupName'] ?? '') === 'msg_tpl_workflow_contribution'
  //   && ($params['valueName'] ?? '') === 'contribution_online_receipt'
  //   && CRM_Utils_Rule::positiveInteger($params['tplParams']['contributionID'] ?? 0)
  // ) {

  // Civi::log()->debug("alterMailParams", ['params' => $params, 'context' => $context]);

  // OK, looks like one we want to stop.
  // Stop it IF it's the first contribution of a recurring one, or it's a one off.
  // We also need to watch out for old style contribution forms which we should leave alone.
  $contribution = civicrm_api3('Contribution', 'getsingle', [
    'return' => ['contribution_page_id', 'contribution_recur_id', 'receive_date'],
    'id'     => $params['tplParams']['contributionID'],
  ]);

  if (!empty($contribution['contribution_page_id'])) {
    // Old style contribution page, leave it alone.
    //Civi::log()->notice(__FUNCTION__ . ' Not altering: belongs to a contribution page.', []);
    return;
  }

  if (!empty($contribution['contribution_recur_id'])) {
    // OK this is a recurring contribution. Is it the first one?
    // it is first, if there are no other contributions before this.
    $count = civicrm_api3('Contribution', 'getcount', [
      'receive_date'          => ['<' => $contribution['receive_date']],
      'contribution_recur_id' => $contribution['contribution_recur_id'],
    ]);
    if ($count) {
      // This is not the first, leave it alone.
      // Civi::log()->notice(__FUNCTION__ . ' Not altering: is a repeat recurring donation.', ['count' => $count]);
      return;
    }
  }

  // OK, do not send.
  Civi::log()->notice(__FUNCTION__ . ' Aborting mail - think it was triggered from a Donation Page which will send it\'s own');
  $params['abortMailSend'] = TRUE;
  // }
}

/**
 * Implements hook_civicrm_unsubscribeGroups()
 *
 * @see https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_unsubscribeGroups/
 */
function oddc_civicrm_unsubscribeGroups($op, $mailingId, $contactId, &$groups, &$baseGroups) {

  // Nb. this hook is caled for GET requests, too, which have yet to be confirmed.
  if ($op === 'unsubscribe' && (($_POST['_qf_Unsubscribe_next'] ?? '') === 'Unsubscribe')) {
    try {
      Civi::log()->info(__FUNCTION__ . " inputs", ['contactId' => $contactId, 'mailingId' => $mailingId, 'groups' => $groups, 'baseGroups' => $baseGroups]);
      $mailing = civicrm_api3('Mailing', 'getsingle', [
        'id' => $mailingId,
        'return' => ["campaign_id.title"],
      ]);
      if (strpos(($mailing['campaign_id.title'] ?? ''), 'FundraisingSend') !== FALSE) {
        // Tag this person.
        Civi::log()->info(
          __FUNCTION__ . " will tag Contact with 'No fundraising emails' because they unsubscribed from mailing",
          ['contactId' => $contactId, 'mailingId' => $mailingId, 'mailing' => $mailing]
        );
        $result = civicrm_api3('EntityTag', 'getcount', [
          'entity_table' => "civicrm_contact",
          'entity_id'    => $contactId,
          'tag_id'       => "No fundraising emails",
        ]);
        if ($result == 0) {
          Civi::log()->info(__FUNCTION__ . " tagging...");
          civicrm_api3('EntityTag', 'create', [
            'entity_table' => "civicrm_contact",
            'entity_id'    => $contactId,
            'tag_id'       => "No fundraising emails",
          ]);
        }
        else {
          Civi::log()->info(__FUNCTION__ . " already tagged");
        }
      }
      else {
        // Debugging.
        // Civi::log()->info(__FUNCTION__ . " will NOT tag contact with 'No fundraising emails' because mailing campaign does not include FundraisingSend", ['contactId' => $contactId, 'mailingId' => $mailingId, 'mailing' => $mailing]);
      }
    }
    catch (\Exception $e) {
      Civi::log()->warning(
        __FUNCTION__ . " failed to find mailing/tag contact",
        [
          'mailingId' => $mailingId,
          'message' => $e->getMessage(),
        ]
          );
    }
  }
}

/**
 * Provide the {currentRegularGiving} token.
 */
function oddc_civicrm_tokens(&$tokens) {
  $tokens['oD'] = [
    'currentRegularGiving' => E::ts('Current regular giving'),
  ];
}

/**
 * Creates the currentRegularGiving token.
 */
function oddc_civicrm_tokenValues(&$values, $cids, $job = NULL, $tokens = array(), $context = NULL) {
  if (empty($tokens['oD'])) {
    return;
  }

  // $tokens is sometimes like: { 'contact': { 'foo': 1 } } and sometimes like { 'contact': ['foo'] }
  if (is_numeric(key($tokens['oD']))) {
    // We have the 2nd form.
    $tokens_in_use = array_values($tokens['oD']);
  }
  else {
    // tokens are keys.
    $tokens_in_use = array_keys($tokens['oD']);
  }

  $contact_ids = [];
  foreach ($cids as $cid) {
    $contact_ids[] = (int) $cid;
  }

  foreach ($contact_ids as $cid) {
    $values[$cid]['oD.currentRegularGiving'] = CRM_Oddc::factory()->getCurrentRegularGivingDescription($cid);
  }
}

/**
 * @param \Civi\API\Event\RespondEvent $e
 */
function oddc__wrap_mailing_preview($event) {
  $apiRequest = $event->getApiRequest();
  if (
    $apiRequest['entity'] !== 'Mailing'
    || $apiRequest['action'] !== 'preview'
  ) {
    return;
  }
  $apiResponse = $event->getResponse();

  // Replace the main visible tokens.
  $apiResponse['values']['body_html'] = preg_replace('/{dearyou.*?}/', 'Dear Supporter', $apiResponse['values']['body_html']);
  $event->setResponse($apiResponse);
}

/**
 * Add some links to the nav menus.
 *
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
function oddc_civicrm_navigationMenu(&$menu) {
  _oddc_civix_insert_navigation_menu($menu, 'Reports', [
    'label' => E::ts('oD Email Dashboard'),
    'name' => 'oddc_email_dashboard',
    'url' => 'civicrm/dashboard/email',
    'permission' => 'access CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _oddc_civix_insert_navigation_menu($menu, 'Reports', [
    'label' => E::ts('oD CiviMail Email Conversions'),
    'name' => 'oddc_mailing_conversion',
    'url' => 'civicrm/odmailingconversion',
    'permission' => 'access CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _oddc_civix_insert_navigation_menu($menu, 'Reports', [
    'label' => E::ts('oD Contribution Explorer'),
    'name' => 'oddc_dataviz_oddd',
    'url' => 'civicrm/dataviz/odd',
    'permission' => 'access CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _oddc_civix_insert_navigation_menu($menu, 'Reports', [
    'label' => E::ts('oD Revenue Dashboard'),
    'name' => 'oddc_revenue_dashboard',
    'url' => 'civicrm/revenuedashboard',
    'permission' => 'access CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _oddc_civix_navigationMenu($menu);
}

/**
 *
 * [InlaySignupPopup]
 */
function oddc_inlaysignup_process_handler($event) {

  // Keep original 'findOrCreate' function which sets contactID on the $data
  unset($event->chain['addContactToGroup']);
  unset($event->chain['sendWelcomeEmail']);
  $event->chain['oDCustom'] = 'oddc_inlaysignup_submission_handler';
}

/**
 * [InlaySignupPopup]
 *
 * @param object $event is like:
 * {
 *   input: { ... }, Validated input (first_name, last_name, email)
 *   inlay: \Civi\Inlay\Type instance,
 * }
 */
function oddc_inlaysignup_submission_handler(array $data, \Civi\Inlay\InlaySignup $inlay) {

  Civi::log()->info(__FUNCTION__, ['=start' => 'signup' . $data['contactID'], 'data' => $data, 'inlayName' => $inlay->getName()]);
  $inlayConfig = $inlay->getConfig();
  $mailingGroupID = (int) ($inlayConfig['mailingGroup'] ?? 0);
  if ($mailingGroupID) {
    $groupName = \Civi\Api4\Group::get(FALSE)
      ->setSelect(['title'])
      ->addWhere('id', '=', $mailingGroupID)
      ->execute()->first()['title'] ?? '(unknown group - deleted?!)';
    $group = "<p>Added to group " . htmlspecialchars($groupName) . " ($mailingGroupID). </p>";
  }
  else {
    $group = '';
  }

  $source = $possiblyTruncatedSource = $data['source'];
  $maxLen = 512;
  if (mb_strlen($source) > $maxLen) {
    $possiblyTruncatedSource = mb_substr($source, 0, $maxLen);
    Civi::log()->warning("Truncated the following on contact $data[contactID]\n"
      . json_encode($source) . "\n"
      . json_encode($possiblyTruncatedSource));
  }

  // Hack 11 Oct 2021 for Matt.
  if (preg_match('@^https://www.opendemocracy.net/en/(north-africa-west-asia|tagged/middle-east-north-africa)/@', $source)) {
    $mailingGroupID = 9; /* Newsletter: North Africa, West Asia */
  }

  // Clear on_hold.
  CRM_Oddc::drySetBestEmail($data['contactID'], $data['email'], TRUE);

  CRM_Oddc::drySignup(
    $data['contactID'],
    $mailingGroupID,
    'Signed up using website pop-up',
    $group . "<p>Title: "
      . htmlspecialchars($inlayConfig['title']) . '</p>'
      . "<p>Intro:</p><blockquote>" . $inlayConfig['introHTML'] . '</blockquote>',
    $possiblyTruncatedSource /* stored in location */
  );

  // Now send special Klaviyo event
  $eventAttributes = [
    'properties' => [
      'groupName'  => $groupName,
      'popupTitle' => $inlayConfig['title'],
      'popupIntro' => $inlayConfig['introHTML'],
      'sourceURL'  => $source,
    ],
  ];
  // Separate out utm params.
  $_ = parse_url($source)['query'] ?? '';
  if ($_) {
    $query = [];
    parse_str($_, $query);
    // Use utm params if passed (they won't be until/unless Wagtail starts sending them).
    foreach (['utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content'] as $_) {
      if (!empty($query[$_])) {
        $eventAttributes['properties'][$_] = $query[$_];
      }
    }
  }
  $glue = Civi\Klaviyo\Glue::singleton();
  $glue->sendEvent('Newsletter signup', $data['contactID'], $eventAttributes);
  Civi::log()->info(__FUNCTION__ . " completed normally.", ['=pop' => 1]);
}

/**
 * Implements hook_civicrm_GoCardlessSubscriptionCancelled
 */
function oddc_civicrm_GoCardlessSubscriptionCancelled(int $contributionRecurID) {
  $needsMigration = 1;
  $result = civicrm_api3('ContributionRecur', 'get', [
    'sequential' => 1,
    'return' => ['contact_id', "amount", "financial_type_id.name", "payment_instrument_id.label", "payment_processor_id.name"],
    'id' => $contributionRecurID,
    'api.Contribution.get' => ['options' => ['limit' => 1], 'return' => ["custom_66", "custom_69"], "sequential" => 1],
  ])['values'][0];

  Civi::log()->info("Sending Cancelled Regular Giving event", ['=start' => '#']);
  $glue = Civi\Klaviyo\Glue::singleton();
  $glue->sendEvent('Cancelled Regular Giving', $result['contact_id'], [
    'unique_id' => 'contribution_recur_id_cancelled_' . $contributionRecurID,
    'properties' => [
      'cancelledMonthlyAmount' => $result['amount'] ?? '',
  // ex. Donation - openDemocracy
      'financialType' => $result["financial_type_id.name"],
      'paymentInstrument' => $result["payment_instrument_id.label"] ?? '',
  // Probably should not need this
      'paymentProcessor' => $result['payment_processor_id.name'] ?? '',
      'oDProject' => $result['api.Contribution.get']['values'][0]['custom_66'] ?? '',
      'donationPageNid' => $result['api.Contribution.get']['values'][0]['custom_69'] ?? '',
    ],
  ]);
  Civi::log()->info("Done", ['=pop' => 1]);
}

/**
 *
 * Event has properties 'inlay' and everything from inlaypay's 'context'
 */
function oddc_inlaypay_checkoutbegin(GenericHookEvent $event) {
  /** @var \Civi\Inlay\Pay */
  $inlay = $event->inlay;
  \Civi::log()->debug("oddc_inlaypay_checkoutbegin", [
    'contactID' => $event->contactID,
    'contributionID' => $event->contributionID,
    'contributionRecurID' => $event->contributionRecurID,
    'processingData' => $event->processingData,
  ]);

  $odProject = $inlay->config['customFields']['od_project_group.od_project'] ?? NULL;
  $categories = empty($odProject) ? [] : [$odProject];
  // Can we add contribution source and campaign?
  $savedContributionData = [];
  if ($event->contributionID) {
    $savedContributionData = Contribution::get(FALSE)
      ->addSelect('campaign_id.title', 'source')
      ->addWhere('id', '=', $event->contributionID)
      ->execute()->first() ?: [];
  }

  // Maps 'card' to, for ex., Debit/Credit card.
  // $paymentService = $inlay->config['devices'][$event->processingData['device']]['label'] ?? '';
  $paymentService = $event->processingData['device'];

  $eventAttributes = ['value' => $event->context['amount']];
  $financialTypeName = \Civi\Api4\FinancialType::get(FALSE)
    ->addWhere('id', '=', $inlay->getFinancialType())
    ->execute()->first()['name'] ?? '';
  $eventProperties = [
    'ItemNames'   => [$financialTypeName],
    'CheckoutURL' => $event->processingData['hostPageUrl'] ?? '',
    'Items'       => [[
      // Civi stuff
      'paymentInstrument' => $paymentService,
      'source'            => $savedContributionData['source'] ?? '',
      'campaign'          => $savedContributionData['campaign_id.title'] ?? '',
      // Klaviyo stuff,
      'kind'              => ($event->processingData['frequency'] === 'oneoff') ? 'One-Off' : 'Recurring',
      'ProductName'       => $financialTypeName,
      'ProductCategories' => $categories,
      'ImageURL'          => 'https://support.opendemocracy.net/sites/default/files/oD_logo.svg',
      'ProductURL'        => $event->processingData['hostPageUrl'] ?? '',
      'RowTotal'          => $event->processingData['amount'],
      'ItemPrice'         => $event->processingData['amount'],
      'Quantity'          => 1,
    ],
    ],
    'Categories'  => $categories,
    // https://developers.klaviyo.com/en/docs/guide_to_integrating_a_subscription_ecommerce_platform#started-checkout
    '$value'      => $event->processingData['amount'],
  ];
  // Can we send utms here?
  foreach (['utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content'] as $field) {
    $eventProperties[$field] = $event->processingData[$field] ?? '';
  }

  $glue = Civi\Klaviyo\Glue::singleton();
  $glue->api->logFullRequest = TRUE;
  $glue->sendEvent('Started Checkout', $event->contactID, $eventAttributes + ['properties' => $eventProperties]);
}

/**
 * The payment has been a success.
 *
 * Event has properties 'inlay', 'common', and everything from inlaypay's 'context'
 * common is an StdClass object with properties:
 * storeGiftAid, storeMessage, storeConsentAndSignup, sendThankYouEmail
 * Set a process to FALSE to prevent it from being executed. These
 * are methods on the Pay class.
 */
function oddc_inlaypay_successpre(GenericHookEvent $event) {
  // We do these things our own way...
  $event->common->sendThankYouEmail = FALSE;
  /** @var Pay $ipay */
  $ipay = $event->inlay;
  if (!$ipay->getConfig()['testMode']) {
    // Use Klaviyo method.
    $event->common->sendThankYouEmail = FALSE;
  }
}

/**
 * The payment has been a success.
 *
 * Called after the 'common' processes in succcesspre.
 * $event contains properties: inlay (the Pay obj) and its context which notably includes:
 *
 * - contactID
 * - contributionID (if successful)
 * - contributionRecurID (ditto, and if recur)
 * - processingData (array of cleaned data, includes 'amount' in default currency)
 */
function oddc_inlaypay_successpost(GenericHookEvent $event) {

  // Ensure their email is the best it can be.
  CRM_Oddc::drySetBestEmail($event->contactID, $event->processingData['email']);

  // If any consent given, record that as an activity.
  $optedIn = FALSE;
  $groups = [];
  foreach ($event->processingData as $k => $v) {
    if (preg_match('/^consent_(\d+)$/', $k, $m) && $v) {
      $optedIn = TRUE;
      $groups[] = $m[1];
    }
  }
  if ($optedIn) {
    // Create an activity.
    // Look up names of mailing lists.
    $groups = \Civi\Api4\Group::get(FALSE)
      ->addWhere('id', 'IN', $groups)
      ->addSelect('title')
      ->execute();
    $listNames = [];
    foreach ($groups as $g) {
      $listNames[] = htmlspecialchars("#$g[id] $g[title]");
    }
    $listNames = implode(', ', $listNames);

    \Civi\Api4\Activity::create(FALSE)
      ->addValue('activity_type_id:name', 'marketing_consent')
      ->addValue('status_id:name', 'Completed')
      ->addValue('target_contact_id', $event->contactID)
      ->addValue('source_contact_id', $event->contactID)
      ->addValue('subject', 'Gave consent at time of making donation.')
      ->addValue('details', '<p>Donation page at '
        . htmlspecialchars($event->processingData['hostPageUrl'] ?? '')
        . '</p>'
        . '<p>Group(s): ' . $listNames . '</p>'
        . '<p>We asked</p><blockquote>' . htmlspecialchars($event->inlay->config['askConsentHTML']) . '</blockquote>')
      ->execute();
  }
}
