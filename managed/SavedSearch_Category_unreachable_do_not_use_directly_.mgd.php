<?php
use CRM_Oddc_ExtensionUtil as E;

return [
  [
    'name' => 'SavedSearch_Category_unreachable_do_not_use_directly_',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Category_unreachable_do_not_use_directly_',
        'label' => E::ts('Category: unreachable (do not use directly)'),
        'api_entity' => 'Contact',
        'api_params' => [
          'version' => 4,
          'select' => ['id'],
          'orderBy' => [],
          'where' => [
            [
              'OR',
              [
                [
                  'NOT',
                  [
                    [
                      'groups:name',
                      'IN',
                      [
                        'General_emails_386',
                      ],
                    ],
                  ],
                ],
                ['is_opt_out', '=', TRUE],
                ['do_not_email', '=', TRUE],
                ['email_primary', 'IS EMPTY'],
                [
                  'contact_type:name',
                  '=',
                  'Organization',
                ],
              ],
            ],
          ],
          'groupBy' => [],
          'join' => [],
          'having' => [],
        ],
      ],
      'match' => ['name'],
    ],
  ],
];
