<?php
use CRM_Oddc_ExtensionUtil as E;

return [
  [
    'name' => 'SavedSearch_Category_interested',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Category_interested',
        'label' => E::ts('Category: interested (do not use directly)'),
        'api_entity' => 'Contact',
        'api_params' => [
          'version' => 4,
          'select' => [
            'id',
            'sort_name',
            'contact_type:label',
            'contact_sub_type:label',
            'Contact_Contribution_contact_id_01.total_amount',
          ],
          'orderBy' => [],
          'groupBy' => [],
          'join' => [
            [
              'Contribution AS Contact_Contribution_contact_id_01',
              'INNER',
              [
                'id',
                '=',
                'Contact_Contribution_contact_id_01.contact_id',
              ],
              [
                'Contact_Contribution_contact_id_01.contribution_status_id:name',
                '=',
                '"Completed"',
              ],
              [
                'Contact_Contribution_contact_id_01.receive_date',
                '>=',
                '"now - 3 month"',
              ],
            ],
          ],
          'having' => [],
        ],
      ],
      'match' => ['name'],
    ],
  ],
];
