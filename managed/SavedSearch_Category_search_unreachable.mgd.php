<?php
use CRM_Oddc_ExtensionUtil as E;

return [
  [
    'name' => 'SavedSearch_Category_search_unreachable',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Category_search_unreachable',
        'label' => E::ts('Category search: unreachable'),
        'api_entity' => 'Contact',
        'api_params' => [
          'version' => 4,
          'select' => [
            'id',
            'sort_name',
            'contact_type:label',
            'contact_sub_type:label',
          ],
          'orderBy' => [],
          'where' => [
            [
              'OR',
              [
                [
                  'contact_type:name',
                  '!=',
                  'Individual',
                ],
                ['do_not_email', '=', TRUE],
                ['is_opt_out', '=', TRUE],
                ['email_primary', 'IS EMPTY'],
                [
                  'NOT',
                  [
                    [
                      'groups:name',
                      'IN',
                      [
                        'General_emails_386',
                      ],
                    ],
                  ],
                ],
              ],
            ],
          ],
          'groupBy' => [],
          'join' => [],
          'having' => [],
        ],
      ],
      'match' => ['name'],
    ],
  ],
];
