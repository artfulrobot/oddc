<?php
use CRM_Oddc_ExtensionUtil as E;

return [
  [
    'name' => 'SavedSearch_Category_cooling_do_not_use_directly_',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Category_cooling_do_not_use_directly_',
        'label' => E::ts('Category: cooling (do not use directly)'),
        'api_entity' => 'Contact',
        'api_params' => [
          'version' => 4,
          'select' => ['id'],
          'orderBy' => [],
          'groupBy' => ['id'],
          'join' => [
            [
              'ContributionRecur AS Contact_ContributionRecur_contact_id_01',
              'INNER',
              [
                'id',
                '=',
                'Contact_ContributionRecur_contact_id_01.contact_id',
              ],
              [
                'OR',
                [
                  [
                    'Contact_ContributionRecur_contact_id_01.end_date',
                    '>=',
                    '"now - 3 month"',
                  ],
                  [
                    'Contact_ContributionRecur_contact_id_01.cancel_date',
                    '>=',
                    '"now - 3 month"',
                  ],
                ],
              ],
              [
                'Contact_ContributionRecur_contact_id_01.contribution_status_id:name',
                'IN',
                ['Completed', 'Failed', 'Cancelled'],
              ],
            ],
            [
              'Contribution AS Contact_Contribution_contact_id_01',
              'INNER',
              [
                'id',
                '=',
                'Contact_Contribution_contact_id_01.contact_id',
              ],
              [
                'Contact_Contribution_contact_id_01.contribution_recur_id',
                '=',
                'Contact_ContributionRecur_contact_id_01.id',
              ],
              [
                'Contact_Contribution_contact_id_01.contribution_status_id:name',
                '=',
                '"Completed"',
              ],
            ],
          ],
          'having' => [],
        ],
      ],
      'match' => ['name'],
    ],
  ],
];
