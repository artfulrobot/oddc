<?php
use CRM_Oddc_ExtensionUtil as E;

return [
  [
    'name' => 'SavedSearch_Contact_Category_Definition_Search_by_Rich_Lott',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Contact_Category_Definition_Search_by_Rich_Lott',
        'label' => E::ts('Category Counts query'),
        'api_entity' => 'ContactCategoryDefinition',
        'api_params' => [
          'version' => 4,
          'select' => [
            'id',
            'label',
            'color',
            'icon',
            'COUNT(ContactCategoryDefinition_ContactCategory_category_definition_id_01.contact_id) AS COUNT_ContactCategoryDefinition_ContactCategory_category_definition_id_01_contact_id',
          ],
          'orderBy' => [],
          'where' => [],
          'groupBy' => ['id'],
          'join' => [
            [
              'ContactCategory AS ContactCategoryDefinition_ContactCategory_category_definition_id_01',
              'LEFT',
              [
                'id',
                '=',
                'ContactCategoryDefinition_ContactCategory_category_definition_id_01.category_definition_id',
              ],
            ],
          ],
          'having' => [],
        ],
      ],
      'match' => ['name'],
    ],
  ],
  [
    'name' => 'SavedSearch_Contact_Category_Definition_Search_by_Rich_Lott_SearchDisplay_Category_counts',
    'entity' => 'SearchDisplay',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Category_counts',
        'label' => E::ts('Category counts'),
        'saved_search_id.name' => 'Contact_Category_Definition_Search_by_Rich_Lott',
        'type' => 'table',
        'settings' => [
          'description' => E::ts('Shows a list of all categories and how many contacts are in each one.'),
          'sort' => [
            ['label', 'ASC'],
          ],
          'limit' => 50,
          'pager' => FALSE,
          'placeholder' => 0,
          'columns' => [
            [
              'type' => 'field',
              'key' => 'label',
              'dataType' => 'String',
              'label' => E::ts('Category'),
              'sortable' => TRUE,
              'rewrite' => '',
              'icons' => [
                [
                  'field' => 'icon',
                  'side' => 'left',
                ],
              ],
              'cssRules' => [],
            ],
            [
              'type' => 'field',
              'key' => 'COUNT_ContactCategoryDefinition_ContactCategory_category_definition_id_01_contact_id',
              'dataType' => 'Integer',
              'label' => E::ts('Count'),
              'sortable' => TRUE,
            ],
          ],
          'actions' => FALSE,
          'classes' => ['table', 'table-striped'],
        ],
      ],
      'match' => [
        'saved_search_id',
        'name',
      ],
    ],
  ],
];
