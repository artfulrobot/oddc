<?php
use CRM_Oddc_ExtensionUtil as E;

return [
  [
    'name' => 'SavedSearch_Category_drifting',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Category_drifting',
        'label' => E::ts('Category: drifting (do not use directly)'),
        'api_entity' => 'Contact',
        'api_params' => [
          'version' => 4,
          'select' => [
            'id',
            'sort_name',
            'contact_type:label',
            'contact_sub_type:label',
            'COUNT(Contact_Contribution_contact_id_01.id) AS COUNT_Contact_Contribution_contact_id_01_id',
          ],
          'orderBy' => [],
          'groupBy' => ['id'],
          'join' => [
            [
              'Contribution AS Contact_Contribution_contact_id_01',
              'INNER',
              [
                'id',
                '=',
                'Contact_Contribution_contact_id_01.contact_id',
              ],
              [
                'Contact_Contribution_contact_id_01.contribution_status_id:name',
                '=',
                '"Completed"',
              ],
            ],
          ],
          'having' => [],
        ],
      ],
      'match' => ['name'],
    ],
  ],
];
