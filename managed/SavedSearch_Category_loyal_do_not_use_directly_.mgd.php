<?php
use CRM_Oddc_ExtensionUtil as E;

return [
  [
    'name' => 'SavedSearch_Category_loyal_do_not_use_directly_',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Category_loyal_do_not_use_directly_',
        'label' => E::ts('Category: loyal (do not use directly)'),
        'api_entity' => 'Contact',
        'api_params' => [
          'version' => 4,
          'select' => [
            'id',
            'sort_name',
            'contact_type:label',
            'contact_sub_type:label',
          ],
          'orderBy' => [],
          'where' => [
            [
              'OR',
              [
                [
                  'Contact_ContributionRecur_contact_id_01.id',
                  'IS NOT EMPTY',
                ],
              ],
            ],
          ],
          'groupBy' => ['id'],
          'join' => [
            [
              'ContributionRecur AS Contact_ContributionRecur_contact_id_01',
              'LEFT',
              [
                'id',
                '=',
                'Contact_ContributionRecur_contact_id_01.contact_id',
              ],
              [
                'Contact_ContributionRecur_contact_id_01.contribution_status_id:name',
                'IN',
                ['In Progress', 'Overdue'],
              ],
              [
                'Contact_ContributionRecur_contact_id_01.amount',
                '<',
                '"20"',
              ],
            ],
          ],
          'having' => [],
        ],
      ],
      'match' => ['name'],
    ],
  ],
];
