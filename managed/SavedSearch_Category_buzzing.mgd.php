<?php
use CRM_Oddc_ExtensionUtil as E;

return [
  [
    'name' => 'SavedSearch_Category_buzzing',
    'entity' => 'SavedSearch',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Category_buzzing',
        'label' => E::ts('Category: buzzing (do not use directly)'),
        'api_entity' => 'Contact',
        'api_params' => [
          'version' => 4,
          'select' => [
            'id',
            'sort_name',
            'contact_type:label',
            'contact_sub_type:label',
          ],
          'orderBy' => [],
          'where' => [
            [
              'OR',
              [
                [
                  'Contact_ContributionRecur_contact_id_01.start_date',
                  '>=',
                  'now - 3 month',
                ],
                [
                  'Contact_ActivityContact_Activity_01.id',
                  'IS NOT EMPTY',
                ],
              ],
            ],
          ],
          'groupBy' => [],
          'join' => [
            [
              'ContributionRecur AS Contact_ContributionRecur_contact_id_01',
              'INNER',
              [
                'id',
                '=',
                'Contact_ContributionRecur_contact_id_01.contact_id',
              ],
              [
                'Contact_ContributionRecur_contact_id_01.contribution_status_id:name',
                'IN',
                ['In Progress', 'Overdue'],
              ],
            ],
            [
              'Activity AS Contact_ActivityContact_Activity_01',
              'LEFT',
              'ActivityContact',
              [
                'id',
                '=',
                'Contact_ActivityContact_Activity_01.contact_id',
              ],
              [
                'Contact_ActivityContact_Activity_01.record_type_id:name',
                '=',
                '"Activity Targets"',
              ],
              [
                'Contact_ActivityContact_Activity_01.activity_type_id:name',
                '=',
                '"Update Recurring Contribution"',
              ],
              [
                'Contact_ActivityContact_Activity_01.activity_date_time',
                '>=',
                '"now - 3 month"',
              ],
            ],
          ],
          'having' => [],
        ],
      ],
      'match' => ['name'],
    ],
  ],
];
